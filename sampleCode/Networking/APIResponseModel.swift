//
//  APIResponseModel.swift
//
import Foundation
/*
 Assuming all time API will return JSON in following format
 {
     "status": true,
     "message": "success",
     "result": {
         "code": 200,
         "data": <T> // T of type Codable
     },
     "responseCode": 200
 }
 */
struct CommonResponseModel<T:Codable>: Codable {
    let status: Bool?
    let message: String?
    let result: CommonResultModel<T>?
    let responseCode: Int?
}

struct CommonResultModel<T:Codable>: Codable {
    let code: Int?
    let message: String?
    let data: T?
}
