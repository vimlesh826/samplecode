//
//  APIEndPointConfigurator.swift
//  


import Foundation
import Alamofire

struct APIEndPointConfigurator {
    
    var path: String = ""
    var parameter: APIParams = APIParams()
    var method: Alamofire.HTTPMethod = .get
    var encoding: ParameterEncoding = URLEncoding.default
    var isBaseUrlNeedToAppend: Bool = true
    var isHeaderTokenRequired: Bool = true
    var isSignInTokenRequired: Bool = false
    var supportOffline = false
    var isUserIdInHeaderRequired = false
    var fullResponse = false
    
    init(endpoint: APIEndpoint) {
        switch endpoint {
        case .recommendedList:
            self.path = "recommendedHabitList?isRecommended=true"
            method = .get
            encoding = URLEncoding.default
            showLoader = true
            isHeaderTokenRequired = true
            
        }
    }
}
