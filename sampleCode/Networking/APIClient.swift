//
//  APIClient.swift
//  

import Foundation
import Alamofire

protocol APIClientDelegate {
    func callGetAPI<T>(view controller : BaseViewController, withEndpoint endpoint: APIEndpoint, data responseType : T, completion: @escaping (_ status:Bool, _ response:Any)-> Void) where T : Codable
}

class BaseViewController : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//MARK: - APIManager
extension BaseViewController : APIClientDelegate {
    func callGetAPI<T>(view controller: BaseViewController, withEndpoint endpoint: APIEndpoint, data responseType: T, completion: @escaping (Bool, Any) -> Void) where T : Codable {
        APIManager.shared.performRequest(withEndpoint: endpoint, responseType: responseType) { status, response in
//            guard let response = response else {return}
            //TODO
        }
    }
}
