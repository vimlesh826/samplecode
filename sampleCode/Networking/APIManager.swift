//
//  APIManager.swift
//

import Alamofire
import UIKit

enum APIError: Error {
    case custom(message: String)
    
}

class APIManager: Alamofire.Session{
    
    internal static let shared: APIManager = {
        let manager = Alamofire.Session.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        return APIManager()
    }()
    
    //MARK: - Request
    func performRequest<T : Codable>(withEndpoint: APIEndpoint ,responseType: T, completionHandler:@escaping (_ status : Bool,_ response: CommonResponseModel<T>?)->Void) {
        AF.request(APIRouter.init(endpoint: withEndpoint))
            .validate(statusCode: 200..<600)
            .responseDecodable(of: CommonResponseModel<T>.self
            ) { response in
                print(response.debugDescription)
                switch response.result {
                case .success (let response):
                    completionHandler(response.status ?? false, response)
                case .failure(let error):
                    let errorString = error.localizedDescription
                    print(errorString)
                    completionHandler(false, nil)
                }
            }
    }
}

