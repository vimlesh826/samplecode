//
//  APIConfiguration.swift
//  

import Foundation
import Alamofire

let resultsPerPageOfAVline = 25
let kStatus = "status"
let kToken = "token"
let kSuccess = "success"
let kMessage = "message"
let kResult = "result"
let kErrorCode = "ErrorCode"
let kAuthToken = "token"
let kCreatedOn = "createdOn"
let kExpiresIn = "expiresIn"
let kData = "data"
let KHeaderToken = "token"


enum ConstantTextsApi: String {
    
    case noInternetConnection = "No Internet Connection"
    case noInternetConnectionTryAgain = "No Internet Connection. Please try again."
    case connecting = "connecting"
    case errorOccurred = "ErrorOccurred"
    case AppName = "myLoqta"
    case serverNotResponding = "Server is not responding."
    case cancel = "Cancel"
    
    var localizedString:String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}
