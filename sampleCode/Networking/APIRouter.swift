//
//  APIRouter.swift
//  ASAPP
//
//  Created by Abichal on 8/8/19.
//  Copyright © 2019 Abichal. All rights reserved.
//

import UIKit
import Alamofire


public typealias JSONDictionary = [String: AnyObject]
typealias APIParams = [String: AnyObject]

struct APIRouter: URLRequestConvertible {
    
    var endpoint: APIEndpoint
    var endPointConfigurator: APIEndPointConfigurator
    init(endpoint: APIEndpoint) {
        self.endpoint = endpoint
        self.endPointConfigurator = APIEndPointConfigurator.init(endpoint: endpoint)
    }
    
    var baseUrl: String {
        return APIEnvironment.shared.baseURL
    }
    
    var method: Alamofire.HTTPMethod {
        return endPointConfigurator.method
    }
    
    var path: String {
        return endPointConfigurator.path
    }
    
    var parameters: APIParams {
        return endPointConfigurator.parameter
    }
    
    var encoding: ParameterEncoding? {
        return endPointConfigurator.encoding
    }
    
//    var showAlert: Bool {
//        return endPointConfigurator.showAlert
//    }
//    
//    var showMessageOnSuccess: Bool {
//        return endPointConfigurator.showMessageOnSuccess
//    }
//    
//    var showLoader: Bool {
//        return endPointConfigurator.showLoader
//    }
//    
    var supportOffline: Bool {
        return endPointConfigurator.supportOffline
    }
    
    var isBaseUrlNeedToAppend: Bool {
        return endPointConfigurator.isBaseUrlNeedToAppend
    }
    
    var isHeaderTokenRequired: Bool {
        return endPointConfigurator.isHeaderTokenRequired
    }
    
    var isSignInTokenRequired: Bool {
        return endPointConfigurator.isSignInTokenRequired
    }
    
    var isUserIdInHeaderRequired: Bool {
        return endPointConfigurator.isUserIdInHeaderRequired
    }
    
    var fullResponse: Bool {
        return endPointConfigurator.fullResponse
    }
    
    /// Returns a URL request or throws if an `Error` was encountered.
    /// - throws: An `Error` if the underlying `URLRequest` is `nil`.
    /// - returns: A URL request.
    
    func asURLRequest() throws -> URLRequest {
        
        let url = URL(string: baseUrl)
        
        var urlRequest = URLRequest(url: (isBaseUrlNeedToAppend ? URL(string: (url?.appendingPathComponent(self.path).absoluteString.removingPercentEncoding?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!)  : URL(string: self.path)) ?? URL(fileURLWithPath: ""))
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60
//        if isSignInTokenRequired {
//            let token = UserSession.shared.getAccessToken()
//            print("the sign in token is --->",token)
//            //urlRequest.setValue("\(token)", forHTTPHeaderField: "token")
//            urlRequest.setValue(token, forHTTPHeaderField: "Authorization")
//        } else if  isHeaderTokenRequired {
//            let token = UserSession.shared.getAccessToken()
//            print("the token is --->",token)
//            urlRequest.setValue(token, forHTTPHeaderField: "Authorization")
//            urlRequest.setValue(TimeZone.current.identifier, forHTTPHeaderField: "timezone")
//            urlRequest.setValue("\(IntegerConstants.kDeviceType)", forHTTPHeaderField: Constants.APIKeys.kDeviceType)
//            urlRequest.setValue(UserSession.shared.getDeviceToken(), forHTTPHeaderField: Constants.APIKeys.kDeviceToken)
//          //urlRequest.setValue("\(token)", forHTTPHeaderField: "token")
//        }
        
        urlRequest.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        return try encoding!.encode(urlRequest, with: self.parameters)
    }
}
