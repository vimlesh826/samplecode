//
//  APIEnvironment.swift
//  


import Foundation

public enum PlistKey: String {
    case socketBaseURL = "SocketBaseURL"
    case baseURL = "BaseURL"
}

class APIEnvironment {
    
    static let shared: APIEnvironment = APIEnvironment()
    
    lazy var baseURL: String = {
        return configuration(.baseURL)
    }()
    
    lazy var socketBaseURL: String = {
        return configurationSocket(.socketBaseURL)
    }()
    
    private var infoDict: [String: Any]  {
        get {
            if let dict = Bundle.main.infoDictionary {
                return dict
            }else {
                fatalError("Plist file not found")
            }
        }
    }
    
    private func configuration(_ key: PlistKey) -> String {
        return infoDict[PlistKey.baseURL.rawValue] as! String
    }
    
    private func configurationSocket(_ key: PlistKey) -> String {
        return infoDict[PlistKey.socketBaseURL.rawValue] as! String
    }
}

